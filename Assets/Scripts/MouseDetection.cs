﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDetection : MonoBehaviour
{

    Color originalColor;
    MeshRenderer renderer;
    
   
    [SerializeField] Color highlightColor, denieHighlighColor;
    Waypoint thisWaypoint;
    TowerFactory towerFactory;
    // Use this for initialization
    void Start()
    {
        towerFactory = FindObjectOfType<TowerFactory>();
        thisWaypoint = transform.GetComponent<Waypoint>();
        renderer = transform.Find("Block_Friendly").GetComponent<MeshRenderer>();
        originalColor = renderer.material.color;
    }
    private void OnMouseOver()
    {
        if (thisWaypoint.isPlaceble)
        {
            renderer.material.color = highlightColor;
        }
        else renderer.material.color = denieHighlighColor;
    }

    private void OnMouseExit()
    {
        renderer.material.color = originalColor;
    }

    private void OnMouseDown()
    {
        if (thisWaypoint.isPlaceble)
        {
            towerFactory.InstantiateTower(thisWaypoint);
           
        }
        
        
    }
    
}
