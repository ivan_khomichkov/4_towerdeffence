﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{
    //parametrs
    [SerializeField] Waypoint StartPoint;


    [SerializeField] Waypoint EndPoint;
    
    //states
    Vector2Int[] allDirections =
     {
            Vector2Int.up,
            Vector2Int.right,
            Vector2Int.down,
            Vector2Int.left,
    };
    Dictionary<Vector2Int, Waypoint> grid = new Dictionary<Vector2Int, Waypoint>();
    bool isRuning = true;
    Waypoint searchCenter;
    Queue<Waypoint> queue = new Queue<Waypoint>();
    List<Waypoint> path = new List<Waypoint>();

    public List<Waypoint> GetPath()
    {
        if (path.Count<1)
        {
            LoadBlocks();
            BreathFirst();
            BuildPath(); 
        }
        return path;
    }
   

    private void BreathFirst()
    {
        queue.Enqueue(StartPoint);
        StartPoint.isExplored = true;
        while (queue.Count > 0 && isRuning)
        {
            searchCenter = queue.Dequeue();
            CheckForEndpoint();
            FindNeighbors();
        }
    }

    private void CheckForEndpoint()
    {
        if (searchCenter.GridPoss == EndPoint.GridPoss)
        {
            isRuning = false;
        }
    }

    private void BuildPath()
    {
        Waypoint last = EndPoint;
        last.isPlaceble = false;
        while (last.GridPoss!= StartPoint.GridPoss)
        {
            path.Add(last);
            last = last.exploredBy;
            last.isPlaceble = false;

        }
        StartPoint.isPlaceble = false;
        path.Add(StartPoint);

        path.Reverse();
    }

    private void FindNeighbors()
    {
        if (!isRuning) { return; }
     
        foreach (var direction in allDirections)
        {
            Vector2Int neighborPos = searchCenter.GridPoss + direction;
            if (grid.ContainsKey(neighborPos))
            {
                QueingNeighbors(neighborPos);
            }
        }
    }

    private void QueingNeighbors(Vector2Int neighborPos)
    {
        Waypoint neighbor = grid[neighborPos];

        if (neighbor.isExplored)
        {
            //do nothing
        }
        else
        {
            neighbor.isExplored = true;
            queue.Enqueue(neighbor);
            neighbor.exploredBy = searchCenter;
        }
    }

    

    private void LoadBlocks()
    {
        var allBlocks = FindObjectsOfType<Waypoint>();
        foreach (var item in allBlocks)
        {
            if (!grid.ContainsKey(item.GridPoss))
            {
                grid.Add(item.GridPoss, item);
            }
            else
            {
                Debug.LogWarning(item.GridPoss + " is found in world more than once.");
            }
        }
    }
}
