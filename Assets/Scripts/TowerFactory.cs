﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour {

    //have to use ring buffer
    //have to instantiate towers maximum of int number
    //when tries to create new more than max number, 
        //move the first one in the new place.

    [SerializeField] int maxNumberOfTowers;

    [SerializeField] Transform towerHighrirhy;
    [SerializeField] TowerScript tower;
    
    Queue<TowerScript> allTowers = new Queue<TowerScript>();
   

    public void InstantiateTower(Waypoint placeForTower)
    {
        int numOftowers = allTowers.Count;
        if (numOftowers<=maxNumberOfTowers) 
        {
            InstantiateNewTower(placeForTower);
        }
        else
        {
            MoveExsistingTower(placeForTower);
        }
  
    }
    private void InstantiateNewTower(Waypoint placeForTower)
    {
        tower.towerWaypoint = placeForTower;
        tower.towerWaypoint.isPlaceble = false;
        var newTower = Instantiate(tower, tower.towerWaypoint.transform.position, Quaternion.identity, towerHighrirhy);
        allTowers.Enqueue(newTower);
        
    }
    private void MoveExsistingTower(Waypoint newWaypoint)
    {
        var oldTower = allTowers.Dequeue();
        oldTower.towerWaypoint.isPlaceble = true;
        newWaypoint.isPlaceble = false;
        oldTower.towerWaypoint = newWaypoint;

        oldTower.transform.position = oldTower.towerWaypoint.transform.position;
        allTowers.Enqueue(oldTower);
    }
}
